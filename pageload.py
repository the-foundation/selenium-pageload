#!/usr/bin/env python
from selenium import webdriver
import argparse
import os
import sys
import time
import random
import logging
# init logging to stdout and file
logger = logging.getLogger('time-load')
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s >>> %(levelname)s: %(message)s')

file_logger = logging.FileHandler('/tmp/time-load.log')
file_logger.setFormatter(formatter)
logger.addHandler(file_logger)

stdout_logger = logging.StreamHandler(sys.stdout)
stdout_logger.setFormatter(formatter)
logger.addHandler(stdout_logger)



def main(logger):
	
	
	
    parser = argparse.ArgumentParser(description='blablablablabla')
    
    parser.add_argument("-u", "--url", dest="URL",
                    help="url to get", metavar="URL")
                    
    parser.add_argument('--browser', default='firefox', help='browser name \
                        must be supported by selenium \
                        (options: firefox, phantomjs')

    parser.add_argument('--verbose', action='store_true',
                        help='show extra output')

    parser.add_argument('--headless', action='store_true', help='initialise a \
                        virtual display for headless systems')
    
    args = parser.parse_args()
    
    
    
    try:
       if (not args.URL):
           raise ImportError
    except ImportError:
       logger.error("NO URL (-u --url)")
       sys.exit(-1)   
    
    URL = args.URL
    
    if args.headless:
        logger.debug('init virtual display')
        from pyvirtualdisplay import Display
        
        display = Display(visible=0, size=(1366, 768))
        display.start()

    logger.debug('init browser')
    if args.browser == 'phantomjs':
        browser = webdriver.PhantomJS()
    else:
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)

        browser = webdriver.Firefox(firefox_profile=profile, log_path="/tmp/picopageload.ff.log")

    browser.get(URL)
    navigationStart = browser.execute_script("return window.performance.timing.navigationStart")
    responseStart = browser.execute_script("return window.performance.timing.responseStart")
    domComplete = browser.execute_script("return window.performance.timing.domComplete")
    loadEnd = browser.execute_script("return performance.timing.loadEventEnd")
    
    backendPerformance = responseStart - navigationStart
    frontendPerformance = domComplete - responseStart
    fullTime = domComplete - navigationStart
    loadEndTime = loadEnd - navigationStart
	
    print "backend:%s" % backendPerformance
    print "frontend:%s" % frontendPerformance
    print "fulltime:%s" % fullTime
    print "loadtime:%s" % loadEndTime
	
    browser.quit()
    display.stop()
    exit_code = 0
    exit(exit_code)

if __name__ == "__main__":
    main(logger)
