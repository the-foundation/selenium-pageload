# Selenium Web page load measurement

* Measures Timings to load the page you specified with -u e.g. `--url https://google.com`
* Uses firefox to go through the site. You may optionally install PhantomJS to use with the `--browser` flag.
* Can be run using a virtual display for headless systems by passing the `--headless` flag.

## Setting Up

To setup your virtualenv (once):

	$ cd selenium-pageload
	$ virtualenv venv

To use it:

	$ source venv/bin/activate
	$ pip install -r requirements.txt
	$ python pageload.py

---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/selenium-pageload/README.md/logo.jpg" width="480" height="270"/></div></a>
